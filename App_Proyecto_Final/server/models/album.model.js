const mongoose = require('mongoose')

const Album = new mongoose.Schema({
    title: {type: String, required: true},
    description: {type: String, required: true},
    dateCreated: {type: Date, required: true, unique: true},
    userId: {type: String, required: true},
}, { collection: 'albums' })

const model = mongoose.model('AlbumData', Album)

module.exports = model