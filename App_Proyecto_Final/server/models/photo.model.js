const mongoose = require('mongoose')

const Photo = new mongoose.Schema({
    title: {type: String, required: true},
    albumId: {type: String, required: true},
    url: {type: String, required: true},
}, { collection: 'photos' })

const model = mongoose.model('PhotoData', Photo)

module.exports = model