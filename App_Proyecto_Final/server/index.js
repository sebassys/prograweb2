const express = require('express')
const app = express()
const cors = require('cors')

app.use(cors())
app.use(express.json())

const userRoutes = require('./routes/user.routes');
const authRoutes = require('./routes/auth.routes');
const albumRoutes = require('./routes/album.routes');
const photoRoutes = require('./routes/photo.routes');

app.use(userRoutes);
app.use(authRoutes);
app.use(albumRoutes);
app.use(photoRoutes);

app.get('/hello', (req, res) => {
    res.send('hello world')
})

app.listen(8080, () => {
     console.log('Server Started on 8080')
})