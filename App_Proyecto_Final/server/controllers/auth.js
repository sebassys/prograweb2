const mongoose = require('mongoose')
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const User = require('../models/user.model')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
mongoose.connect('mongodb://localhost:27017/Portfolio')

const transporter = nodemailer.createTransport(sendgridTransport({
    auth: {
        api_key: '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'
    }
}))

exports.postUser = async (req, res) => {
    try {
        const newPassword = await bcrypt.hash(req.body.password, 10)
        await User.create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: newPassword,
            birthday: req.body.birthday,
            gender: req.body.gender,
        })
        res.json({ status: 'ok'})
    } catch (err) {
        res.json({ status: 'error', user: false})
    }
}

exports.postLogin = async (req, res) => {
    const user = await User.findOne({
        email: req.body.email,
    })
    if(user){
        const isPasswordValid = await bcrypt.compare(req.body.password, user.password)
        if (isPasswordValid) {
            const token = jwt.sign({
                email: user.email,
                firstName: user.firstName,
            }, 'secret123')
        return res.json({ status: 'ok', user: token })
        } else {
            return res.json({ status: 'error', user: false })
        }
    } else {
        return res.json({ status: 'error', user: false })
    }
}

exports.sendPassword = async (req, res) => {
    const user = await User.findOne({
        email: req.body.email,
    })
    if(user){
        transporter.sendMail({
            to: user.email,
            from: 'sebassys@gmail.com',
            subject: 'Porfolio Pass Recover',
            html: `<h1>Hi ${user.firstName}</h1><p>This is your password: <strong>${user.password}</strong></p>`
        })
        return res.json({ status: 'ok'})
    } else {
        return res.json({ status: 'error'})
    }
}