const mongoose = require('mongoose')
const User = require('../models/user.model')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
mongoose.connect('mongodb://localhost:27017/Portfolio')

exports.getUser = async (req, res) => {
    const token = req.headers['x-access-token']
    try {
        const decoded = jwt.verify(token, 'secret123')
        const email = decoded.email
        const user = await User.findOne({ email: email })
        return res.json({ status: 'ok', user: user })
    } catch (error) {
        res.json({ status: 'error', error: 'invalid token' })
    }
}

exports.updateUser = async (req, res) => {
    const token = req.headers['x-access-token']
    try {
        const decoded = jwt.verify(token, 'secret123')
        const email = decoded.email
        const user = await User.findOne({ email: email })
        const userEmail = await User.findOne({ email: req.body.email })
        if(!userEmail){
            const newPassword = await bcrypt.hash(req.body.password, 10)
            await User.updateOne({_id: user._id }, {
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: newPassword,
                birthday: req.body.birthday,
                gender: req.body.gender,
            })
            res.json({ status: 'ok'})
        } else {
            if(!userEmail._id === user._id){
                const newPassword = await bcrypt.hash(req.body.password, 10)
                await User.updateOne({_id: user._id }, {
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    email: req.body.email,
                    password: newPassword,
                    birthday: req.body.birthday,
                    gender: req.body.gender,
                })
                res.json({ status: 'ok'})
            } else {
                res.json({ status: 'False'})
            }
        }
    } catch (err) {
        res.json({ status: 'error', user: false})
    }
}