const mongoose = require('mongoose')
const Album = require('../models/album.model')
const User = require('../models/user.model')
const jwt = require('jsonwebtoken')
mongoose.connect('mongodb://localhost:27017/Portfolio')

exports.getAlbums = async (req, res) => {
    const token = req.headers['x-access-token']
    try {
        const decoded = jwt.verify(token, 'secret123')
        const email = decoded.email
        const user = await User.findOne({ email: email })
        if(user){
            const albums = await Album.find({ userId: user._id })
            return res.json({ status: 'ok', albums: albums })
        } else {
            return res.json({ status: 'error', albums: false })
        }
    } catch (error) {
        res.json({ status: 'error', error: 'invalid token' })
    }
}

exports.postAlbum = async (req, res) => {
    const token = req.headers['x-access-token']
    try {
        const decoded = jwt.verify(token, 'secret123')
        const email = decoded.email
        const user = await User.findOne({ email: email })
        const date = new Date(+new Date() + 7*24*60*60*1000)
        if(user){
            await Album.create({
                title: req.body.title,
                description: req.body.description,
                dateCreated: date,
                userId: user._id,
            })
            res.json({ status: 'ok'})
        } else {
            return res.json({ status: 'error', user: false })
        }
    } catch (err) {
        res.json({ status: 'error', album: false})
    }
}

exports.postUpdateAlbum = async (req, res) => {
    try {
        await Album.updateOne({_id: req.body.albumId }, {
            title: req.body.title,
            description: req.body.description,
        })
        return res.json({ status: 'ok'})
    } catch (err) {
        res.json({ status: 'error', photo: false})
    }
}

exports.postDeleteAlbum = async (req, res) => {
     try {
        const album = await Album.findByIdAndDelete({
            _id: req.body.albumId,
        })
        if(album){
            await Photo.deleteMany({
                albumId: req.body.albumId,
            })
            return res.json({ status: 'ok'})
        } else {
            return res.json({ status: 'error', album: false })
        }
    } catch (err) {
        res.json({ status: 'error', albumId: false})
    }
}