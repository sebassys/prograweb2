const mongoose = require('mongoose')
const Photo = require('../models/photo.model')
const User = require('../models/user.model')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
mongoose.connect('mongodb://localhost:27017/Portfolio')

exports.getPhotos = async (req, res) => {
    const albumId = req.params.albumId
    try {
        const photos = await Photo.find({albumId: albumId})
        return res.json({ status: 'ok', photos: photos })
    } catch (error) {
        res.json({ status: 'error', error: 'invalid token' })
    }
}

exports.postPhoto = async (req, res) => {
    try {
        await Photo.create({
            title: req.body.title,
            url: req.body.url,
            albumId: req.body.albumId,
        })
        return res.json({ status: 'ok'})
    } catch (err) {
        res.json({ status: 'error', photo: false})
    }
}

exports.UpdatePhoto = async (req, res) => {
    try {
        await Photo.updateOne({_id: req.body.photoId }, {
            title: req.body.title,
            url: req.body.url,
        })
        return res.json({ status: 'ok'})
    } catch (err) {
        res.json({ status: 'error', photo: false})
    }
}

exports.postDeletePhoto = async (req, res) => {
    try {
         await Photo.findByIdAndDelete({
              _id: req.body.photoId,
         })
         return res.json({ status: 'ok'})
    } catch (err) {
        res.json({ status: 'error', photoId: false})
    }
}