const express = require('express');

const authController = require('../controllers/auth');

const router = express.Router();

router.post('/api/register', authController.postUser);
router.post('/api/login', authController.postLogin);
router.post('/api/password', authController.sendPassword);

module.exports = router;