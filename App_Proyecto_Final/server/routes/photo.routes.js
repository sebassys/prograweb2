const express = require('express');

const photoController = require('../controllers/photo');

const router = express.Router();

router.get('/api/getPhotos/:albumId', photoController.getPhotos);
router.post('/api/postPhoto', photoController.postPhoto);
router.post('/api/updatePhoto', photoController.UpdatePhoto);
router.post('/api/postDeletePhoto', photoController.postDeletePhoto);

module.exports = router;