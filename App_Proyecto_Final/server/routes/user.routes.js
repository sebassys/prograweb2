const express = require('express');

const userController = require('../controllers/user');

const router = express.Router();

router.get('/api/user', userController.getUser);
router.post('/api/updateUser', userController.updateUser);

module.exports = router;