const express = require('express');

const albumController = require('../controllers/album');

const router = express.Router();

router.get('/api/getAlbums', albumController.getAlbums);
router.post('/api/postAlbum', albumController.postAlbum);
router.post('/api/postDeleteAlbum', albumController.postDeleteAlbum);
router.post('/api/updateAlbum', albumController.postUpdateAlbum);

module.exports = router;