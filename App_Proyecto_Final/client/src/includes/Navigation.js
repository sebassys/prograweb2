function Navigation(props) {

    function logOut(event) {
        event.preventDefault()
        localStorage.removeItem('token')
        window.location.href = '/'
    }

    return (
        <nav className="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div className="container">
                <a className="navbar-brand" href="#pageTop">Welcome !</a>
                <button className="navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ms-auto">
                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded" href="/portfolio">Home</a>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded" href="/album">Add Album
                            </a>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded" href="/albums">Admin Albums
                            </a>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <a className="nav-link py-3 px-0 px-lg-3 rounded" href="/user">Admin User</a>
                        </li>
                        <li className="nav-item mx-0 mx-lg-1">
                            <button className="btn btn-primary btn-xl btn-outline-light" onClick={(e) => logOut(e)}>Logout</button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
  }
  
  export default Navigation;