import logo from '../images/logo.svg';

function Header(props) {
    return (
        <header id="pageTop" className="masthead bg-primary text-white text-center">
            <div className="container d-flex align-items-center flex-column">
                <img src={logo} className="App-logo2 logo" alt="logo" />
                <h1 className="masthead-heading text-uppercase mb-0">Welcome: <span className="userFirstName">{props.firstName}</span></h1>
            </div>
        </header>
    );
}

export default Header;
