function Footer() {
    return (
        <div>
            <footer className="footer text-center">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <h4 className="text-uppercase mb-4">
                                POGRAMACI&Oacute;N EN AMBIENTE WEB II 
                            </h4>
                        </div>
                    </div>
                </div>
            </footer>
            <div className="copyright py-4 text-center text-white">
                <div className="container"><small>Universidad T&eacute;cnica Nacional 2021</small></div>
            </div>
        </div>
    );
}

export default Footer;