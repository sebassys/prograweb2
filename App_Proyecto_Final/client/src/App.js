import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { React, useEffect, useState } from "react";
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import Login from './pages/Login';
import Register from './pages/Register';
import Navigation from './includes/Navigation';
import Header from './includes/Header';
import Footer from './includes/Footer';
import Portfolio from './pages/Portfolio';
import Album from './pages/Album';
import Albums from './pages/Albums';
import Photos from './pages/Photos';
import AdminUser from './pages/User';
import Recover from './pages/Recover';

const App = () => {

  const [User, setUser] = useState(true)
  const [loggedIn, setLoggedIn] = useState(false)
  const [userInfo, setUserInfo] = useState('')

  async function getUserName() {
        const req = await fetch('http://localhost:8080/api/user', {
            headers: {
                'x-access-token': localStorage.getItem('token'),
            }
        })
        const data = await req.json()
        if(data.status === 'ok') {
            setUserInfo(data.user)
            setLoggedIn(true)
        } else {
            alert(data.error)
        }
  }

  useEffect(() => {
    const token = localStorage.getItem('token')
    if(token === null) {
      setUser(false)
    } else {
      getUserName()
    }
  }, [])

  return (
    <div>
      <BrowserRouter>
      <Route exact path="/login">
        {loggedIn ? <Redirect to="/" /> : <Login />}
      </Route>
      <Route exact path="/register">
        {loggedIn ? <Redirect to="/" /> : <Register />}
      </Route>
      <Route exact path="/recover">
        {loggedIn ? <Redirect to="/" /> : <Recover />}
      </Route>
      {User &&
        <div>
          <Navigation />
          <Header 
            firstName = {userInfo.firstName}
          />
          <Route exact path="/" component={Portfolio} />
          <Route exact path="/portfolio" component={Portfolio} />
          <Route exact path="/album" component={Album} />
          <Route exact path="/albums" component={Albums} />
          <Route exact path="/photos" component={Photos} />
          <Route exact path="/user" component={AdminUser} />
        </div>
      }
      </BrowserRouter>
      {User &&
        <div>
          <Footer />
        </div>
      }
    </div>
  )
}

export default App
