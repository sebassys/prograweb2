import { useEffect, useState } from "react";

function Photos() {

    const [photos, setPhotos] = useState('')
    const [albumId, setAlbumId] = useState('')
    const [title, setTitle] = useState('')
    const [url, setUrl] = useState('')
    const [photoEdit, setPhotoEdit] = useState('')
    const [editPhoto, setEditPhoto] = useState('')

    async function getPhotos(queryParams) {
        const albumId = queryParams.get('id')
        const fetchUrl = 'http://localhost:8080/api/getPhotos/' + albumId
        const req = await fetch(fetchUrl, {
            headers: {
                'Content-Type': 'application/json',
            }
        })
        const data = await req.json()
        if(data.status === 'ok') {
            setPhotos(data.photos)
            setAlbumId(albumId)
        } else {
            alert('Error Get Photo')
        }

    }

    async function postPhoto(event) {
        event.preventDefault()
        const response = await fetch('http://localhost:8080/api/postPhoto', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title,
                url,
                albumId,
            })
        })
        const data = await response.json()
        if(data.status === 'ok') {
            window.location.reload(false)
        } else {
            alert('Error Post Photo')
        }
    }

    function getPhotoEdit(id) {
        for (let i = 0;i<photos.length;i++) {
            if(photos[i]._id === id){
                setPhotoEdit(photos[i])
                setEditPhoto(true)
            }
        }
    }

    function updatePhoto(id) {
        const photoId = id;
        const title = photoEdit.title;
        const url = photoEdit.url;
        const response = fetch('http://localhost:8080/api/updatePhoto', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title,
                url,
                photoId,
            })
        })
        const data = response
        if(data) {
            window.location.reload(false)
        } else {
            alert('Error Update Photo')
        }
    }

    function postDeletePhoto(id) {
        const photoId = id;
        const response = fetch('http://localhost:8080/api/postDeletePhoto', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                photoId,
            })
        })
        const data = response
        if(data) {
            window.location.reload(false)
        } else {
            alert('Error Delete Photo')
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(token === null) {
            window.location.href = '/login'
        } else {
            const queryParams = new URLSearchParams(window.location.search);
            getPhotos(queryParams)
            
        }
    }, [editPhoto])
    
    return (
        <div className="page-section portfolio" id="portfolio">
            <div className="container">
            <h2 className="page-section-heading text-center text-uppercase text-secondary">Photos</h2>
            <div className="row text-center justify-content-center photosView">
                {photos && photos.map(photo => {
                    return (
                    <div className="col-md-6 col-lg-4 mb-5">
                        <img src={photo.url} className="imgAlbum rounded mx-auto d-block" alt=""/>
                        <h4 className="text-center text-secondary">{photo.title}</h4>
                        <div>
                            <button className="btn btn-primary btn-xl btn-outline-light" onClick={() => getPhotoEdit(photo._id)}>Edit Album</button>
                        </div>
                        <div>
                            <button className="btn btn-primary btn-xl btn-outline-light" onClick={() => postDeletePhoto(photo._id)}>Delete Photo</button>
                        </div>
                    </div>
                )})}
                {!photos &&
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">No Photos Found!</h2>
                }
            </div>
            {editPhoto &&
                <div>
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">Edit Photo</h2>
                    <div className="row justify-content-center">
                        <div className="col-lg-8 col-xl-7">
                            <div className="form-floating mb-3">
                                <input className="form-control" name="title" id="title" type="text" placeholder="Enter a Photo Name..." value={photoEdit.title} onChange={(e) => setPhotoEdit({...photoEdit, title: e.target.value})} required/>
                                <label>Photo Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input className="form-control" name="url" id="url" type="text" placeholder="Enter a Photo URL..." value={photoEdit.url} onChange={(e) => setPhotoEdit({...photoEdit, url: e.target.value})} required/>
                                <label>Photo URL</label>
                            </div>
                            <button className="btn btn-primary btn-xl" onClick={() => updatePhoto(photoEdit._id)}>Edit Photo</button>
                        </div>
                    </div>
                </div>
            }
            <h2 className="page-section-heading text-center text-uppercase text-secondary">Add Photo</h2>
                <div className="row justify-content-center">
                    <div className="col-lg-8 col-xl-7">
                        <form className="addAlbumForm" onSubmit={postPhoto}>
                            <div className="form-floating mb-3">
                                <input className="form-control" name="title" id="title" type="text" placeholder="Enter a Photo Name..." onChange={(e) => setTitle(e.target.value)} required/>
                                <label>Photo Title</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input className="form-control" name="url" id="url" type="text" placeholder="Enter a Photo URL..." onChange={(e) => setUrl(e.target.value)} required/>
                                <label>Photo URL</label>
                            </div>
                            <input type="hidden" value={albumId} name="albumId" />
                            <button className="btn btn-primary btn-xl" type="submit">Add Photo</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}
            
export default Photos;