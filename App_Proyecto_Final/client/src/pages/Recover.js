import logo from '../images/logo.svg';
import {useState} from 'react';

function Recover() {
    const [email, setEmail] = useState('')
    const [emailError, setEmailError] = useState(false)
    
    async function sendPassword(event) {
        event.preventDefault()
        const response = await fetch('http://localhost:8080/api/password', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
            })
        })
        const data = await response.json()
        if(data.status === 'ok') {
            alert('Password sent to your email')
            window.location.href = '/login'
        } else {
            setEmailError(true)
        }
    }

    return (
        <div className="LoginWrapper fadeInDown">
            <div id="formContent">
                <div className="fadeIn first">
                    <img src={logo} className="App-logo logo" alt="logo" />
                </div>
                <form className="login" onSubmit={sendPassword}>
                    <input type="text" className="fadeIn third" name="email" placeholder="Email" onChange={(e) => setEmail(e.target.value)}required/>
                    <div className="alert">
                        We will send the password to your email account
                    </div>
                    <input type="submit" className="fadeIn fourth" value="Submit" />
                </form>
                <div className="registerLink">
                    <a href="/login">Back to Login</a>
                </div>
                {emailError &&
                    <div className="alert alert-danger" role="alert">
                        The email you entered do not exist in our system
                    </div>
                }    
            </div>
        </div>
    );
  }
  export default Recover;
  