import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import albumIcon from '../images/albumIcon.png';

function Portfolio() {
    
    const history = useHistory()
    const [albums, setAlbums] = useState('')

    async function getAlbums() {
        const req = await fetch('http://localhost:8080/api/getAlbums', {
            headers: {
                'x-access-token': localStorage.getItem('token'),
            }
        })
        const data = await req.json()
        if(data.status === 'ok') {
            setAlbums(data.albums)
        } else {
            alert(data.error)
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(token === null) {
            history.replace('/login')
        } else {
            getAlbums()
        }
    }, [])
    
    return (
            <section className="page-section portfolio" id="portfolio">
                <div className="container">
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">Album's List</h2>
                    <div className="row justify-content-center">
                        {albums && albums.map(album => {
                        return ( 
                        <div className="col-md-6 col-lg-4 mb-5 indexAlbum">
                            <a href={"/photos/?id=" + album._id}>
                                <img src={albumIcon} className="imgAlbum rounded mx-auto d-block" alt=""/>
                            </a>
                            <h4 className="text-center text-secondary">{album.title}</h4>
                            <p className="text-center text-secondary">{album.description}</p>
                        </div>
                        )
                        })}
                        {!albums && 
                            <h2 className="page-section-heading text-center text-uppercase text-secondary">No Albums Found!</h2>
                        }
                    </div>
                </div>
            </section>
    );
}
        
export default Portfolio;