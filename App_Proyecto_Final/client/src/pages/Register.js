import logo from '../images/logo.svg';
import { useState } from 'react';
import { useHistory } from 'react-router-dom'

function Register() {

    const history = useHistory()

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [birthday, setBirthday] = useState('')
    const [gender, setGender] = useState('')
    const [User, setUser] = useState('true')
    
    async function registerUser(event) {
        event.preventDefault()
        const response = await fetch('http://localhost:8080/api/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password,
                birthday,
                gender,
            })
        })
        const data = await response.json()
        if(data.status === 'ok') {
            history.push('/login')
        } else {
            setUser(data.user)
        }
    }

    return (
        <div className="LoginWrapper fadeInDown" >
            <div id="formContent">
                <div className="fadeIn first">
                    <img src={logo} className="App-logo logo" alt="logo" />
                </div>
                <form className="login" onSubmit={registerUser}>
                    <input type="text" className="fadeIn second" name="firstName" placeholder="First Name" onChange={(e) => setFirstName(e.target.value)} required/>
                    <input type="text" className="fadeIn second" name="lastName" placeholder="Last Name" onChange={(e) => setLastName(e.target.value)} required/>
                    <input type="email" className="fadeIn third" name="email" placeholder="Email" onChange={(e) => setEmail(e.target.value)}required/>
                    <input type="password" className="fadeIn third" name="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} required/>
                    <label>Birthday:</label>
                    <input type="date" name="birthday" min="1900-01-01" max="2021-12-31" onChange={(e) => setBirthday(e.target.value)} required/> 
                    <label> Select you gender</label>
                    <select name="gender" onChange={(e) => setGender(e.target.value)} required>
                        <option value="" selected disabled hidden>Choose here</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">other</option>
                    </select>
                    <input type="submit" className="fadeIn fourth" value="Register" />
                    <div className="registerLink">
                        <a href="/login">Back to Login</a>
                    </div>
                </form>
                {!User &&
                    <div className="alert alert-danger" role="alert">
                        Email address already in use on another&nbsp;account
                        <div className="backToLoginLink">
                            <a href="/login">Back to Login</a>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
  }
  export default Register;
  