import { useState } from 'react';

function Album() {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    
    async function postAlbum(event) {
        event.preventDefault()
        const response = await fetch('http://localhost:8080/api/postAlbum', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token'),
            },
            body: JSON.stringify({
                title,
                description,
            })
        })
        const data = await response.json()
        if(data.status === 'ok') {
            window.location.href = '/portfolio'
        } else {
            alert('error post album')
        }
    }

    return (
        <div className="page-section portfolio">
            <h2 className="page-section-heading text-center text-uppercase text-secondary">Add Album</h2>
            <div className="row justify-content-center">
                <div className="col-lg-8 col-xl-7">
                    <form className="addAlbumForm" onSubmit={postAlbum}>
                        <div className="form-floating mb-3">
                            <input className="form-control" name="title" id="title" type="text" placeholder="Enter an Album Title" onChange={(e) => setTitle(e.target.value)} required />
                            <label>Album Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea className="form-control" name="description" id="description" type="text" placeholder="Enter an Album Description" onChange={(e) => setDescription(e.target.value)} required></textarea>
                            <label>Album Description</label>
                        </div>
                        <button className="btn btn-primary btn-xl" type="submit">Add Album</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
            
export default Album;