import logo from '../images/logo.svg';
import {useState} from 'react';

function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [User, setUser] = useState('true')
    
    async function loginUser(event) {
        event.preventDefault()
        const response = await fetch('http://localhost:8080/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
            })
        })
        const data = await response.json()
        if(data.user) {
            localStorage.setItem('token', data.user)
            window.location.href = '/portfolio'
        } else {
            setUser(data.user)
        }
    }

    return (
        <div className="LoginWrapper fadeInDown">
        <div id="formContent">
            <div className="fadeIn first">
                <img src={logo} className="App-logo logo" alt="logo" />
            </div>
            <form className="login" onSubmit={loginUser}>

                <input type="text" className="fadeIn third" name="email" placeholder="Email" onChange={(e) => setEmail(e.target.value)}required/>
                <input type="password" className="fadeIn third" name="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} required/>

                <input type="submit" className="fadeIn fourth" value="Log In" />
            </form>
            <div className="registerLink">
                <a href="/register">Register</a>
            </div>
            <div className="registerLink">
                <a href="/recover">forgot my password</a>
            </div>
            {!User &&
                <div className="alert alert-danger" role="alert">
                    User do not exist or your credentials are incorrect
                </div>
            }    
        </div>
    </div>
    );
  }
  export default Login;
  