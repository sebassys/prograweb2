import { useEffect, useState } from "react";

function User() {

    const [userEdit, setUserEdit] = useState('')
    const [email, setEmail] = useState(false)

    async function getUserEdit() {
        const req = await fetch('http://localhost:8080/api/user', {
            headers: {
                'x-access-token': localStorage.getItem('token'),
            }
        })
        const data = await req.json()
        if(data.status === 'ok') {
            setUserEdit(data.user)
        } else {
            alert(data.error)
        }
    }

    async function updateUser(event) {
        event.preventDefault()
        const firstName = userEdit.firstName;
        const lastName = userEdit.lastName;
        const email = userEdit.email;
        const password = userEdit.password;
        const birthday = userEdit.birthday;
        const gender = userEdit.gender;
        const response = await fetch('http://localhost:8080/api/updateUser', {
            method: 'POST',
            headers: {
                'x-access-token': localStorage.getItem('token'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                firstName,
                lastName,
                email,
                password,
                birthday,
                gender,
            })
        })
        const data = await response.json()
        if(data.status === 'ok') {
            alert('Info updated. Please login again')
            localStorage.removeItem('token')
            window.location.href = '/'
            
        } else {
            setEmail(true)
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(token === null) {
            window.location.href = '/login'
        } else {
            if(!userEdit) {
                getUserEdit()
            }
        }
    }, [])

    return (
        <div className="page-section portfolio">
            <div className="container">
                <div id="edit">
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">Edit User</h2>
                    <div className="row justify-content-center">
                        <div className="col-lg-8 col-xl-7">
                            <form className="addAlbumForm" onSubmit={updateUser}>
                                <div className="form-floating mb-3">
                                    <input className="form-control" name="firstName" id="firstName" type="text" placeholder="Enter your First Name" value={userEdit.firstName} onChange={(e) => setUserEdit({...userEdit, firstName: e.target.value})} />
                                    <label>First name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input className="form-control" name="lastName" id="lastName" type="text" placeholder="Enter you Last Name" value={userEdit.lastName} onChange={(e) => setUserEdit({...userEdit, lastName: e.target.value})} />
                                    <label>Last Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input className="form-control" name="email" id="email" type="text" placeholder="Enter your Email" value={userEdit.email} onChange={(e) => setUserEdit({...userEdit, email: e.target.value})} />
                                    <label>Email</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input className="form-control" name="password" id="password" type="password" placeholder="Enter your Password" onChange={(e) => setUserEdit({...userEdit, password: e.target.value})} required/>
                                    <label>Password</label>
                                </div>
                                <label><span classname="adminUser">Birthday:</span></label>
                                <input type="date" name="birthday" min="1900-01-01" max="2021-12-31" onChange={(e) => setUserEdit({...userEdit, birthday: e.target.value})}/> 
                                <label> Select you gender</label>
                                <select name="gender" value={userEdit.gender} onChange={(e) => setUserEdit({...userEdit, gender: e.target.value})}>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">other</option>
                                </select>
                                <button className="btn btn-primary btn-xl"  type="submit">Edit User</button>
                            </form>
                            {email &&
                                <div className="alert alert-danger" role="alert">
                                    Looks Like the email you entered is already in use by another accout
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
            
export default User;