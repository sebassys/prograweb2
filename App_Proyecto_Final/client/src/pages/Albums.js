import { useEffect, useState } from "react";
import albumIcon from '../images/albumIcon.png';

function Albums() {

    const [albums, setAlbums] = useState('')
    const [albumEdit, setAlbumEdit] = useState('')
    const [editAlbum, setEditAlbum] = useState('')

    async function getAlbums() {
        const req = await fetch('http://localhost:8080/api/getAlbums', {
            headers: {
                'x-access-token': localStorage.getItem('token'),
            }
        })
        const data = await req.json()
        if(data.status.length > 0) {
            setAlbums(data.albums)
        } else {
            alert(data.error)
        }
    }

    function getAlbumEdit(id) {
        for (let i = 0;i<albums.length;i++) {
            if(albums[i]._id === id){
                setAlbumEdit(albums[i])
                setEditAlbum(true)
            }
        }
    }

    function updateAlbum(id) {
        const albumId = id;
        const title = albumEdit.title;
        const description = albumEdit.description;
        const response = fetch('http://localhost:8080/api/updateAlbum', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                title,
                description,
                albumId,
            })
        })
        const data = response
        if(data) {
            window.location.reload(false)
        } else {
            alert('Error Update Album')
        }
    }

    function postDeleteAlbum(id) {
        const albumId = id
        const response = fetch('http://localhost:8080/api/postDeleteAlbum', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                albumId,
            })
        })
        const data = response
        if(data) {
            window.location.reload(false);
        } else {
            alert('error delete album')
        }
    }

    useEffect(() => {
        const token = localStorage.getItem('token')
        if(token === null) {
            window.location.href = '/login'
        } else {
            if(!albums) {
                getAlbums()
            }
        }
    }, [editAlbum])

    return (
        <div className="page-section portfolio">
            <div className="container">
            <h2 className="page-section-heading text-center text-uppercase text-secondary">Album's List</h2>
            <div className="row text-center justify-content-center">
                {albums && albums.map(album => {
                return (
                    <div className="col-md-6 col-lg-4 mb-5 adminAlbums">
                        <img src={albumIcon} className="imgAlbum rounded mx-auto d-block" alt=""/>
                        <h4 className="text-center text-secondary">{album.title}</h4>
                        <div>
                            <button className="btn btn-primary btn-xl btn-outline-light" onClick={() => getAlbumEdit(album._id)}>Edit Album</button>
                        </div>
                        <div>
                            <button className="btn btn-primary btn-xl btn-outline-light" onClick={() => postDeleteAlbum(album._id)}>Delete Album</button>
                        </div>
                    </div>
                )})}
                {!albums && 
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">No Albums Found!</h2>
                }
            </div>
            {editAlbum &&
                <div id="edit">
                    <h2 className="page-section-heading text-center text-uppercase text-secondary">Edit Album</h2>
                    <div className="row justify-content-center">
                        <div className="col-lg-8 col-xl-7">
                            {albumEdit &&
                            <div>
                                <div className="form-floating mb-3">
                                    <input className="form-control" name="title" id="title" type="text" placeholder="Enter an Album Title" value={albumEdit.title} onChange={(e) => setAlbumEdit({...albumEdit, title: e.target.value})} required />
                                    <label>Album Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <textarea className="form-control" name="description" id="description" type="text" placeholder="Enter an Album Description" value={albumEdit.description} onChange={(e) => setAlbumEdit({...albumEdit, description: e.target.value})} required></textarea>
                                    <label>Album Description</label>
                                </div>
                                <button className="btn btn-primary btn-xl" onClick={() => updateAlbum(albumEdit._id)}>Edit Album</button>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            }
            </div>
        </div>
    );
}
            
export default Albums;